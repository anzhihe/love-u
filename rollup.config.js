import babel from 'rollup-plugin-babel';
import { uglify } from 'rollup-plugin-uglify';

export default {
	input: 'index.js',
	plugins: [
		babel({
			exclude: 'node_modules/**'
		}),
		(process.env.npm_lifecycle_script.endsWith('--environment=prod') && uglify())
	],
	output: [
		{
			file: 'dist/iloveu.esm.js',
			format: 'esm'
		},
		{
			exports: "auto",
			file: 'dist/iloveu.cjs.js',
			format: 'cjs'
		},
		{
			name: "_",
			file: 'dist/iloveu.js',
			format: 'iife'
		}
	]
}
