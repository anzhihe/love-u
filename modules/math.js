/** 数组模块 */

/**
 * @name: rnd
 * @cname: 随机数
 * @desc: 以最大值和最小值获得随机数
 * @param {*} max min
 * @panme: max=10 min=0
 * @result: 4
*/
export function rnd(max, min = 0) {
	return Math.round(Math.random() * (max - min)) + min
}

/**
 * @name: isLeapYear
 * @cname: 判断是否是闰年
 * @desc: 以传入的arr值判断是否是闰年,正确为true 否为flase
 * @param {*} year	 
 * @panme: year=2021
 * @result: flase
*/
export function isLeapYear(year) {
	return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0
}

/**
 * @name: toENChar
 * @cname: 数字转换字母	
 * @desc: 输入数字判断下标为该数字的字母
 * @param {*} val
 * @panme: val=20
 * @result: u
*/
export function toENChar(val) {
	return 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'[val]
}

/**
 * @name: isOdd
 * @cname: 判断是否是奇数
 * @desc: 以传入的数字判断是否为奇数 正确为true 否为flase
 * @param {*} num 
 * @panme: num=20
 * @result: flase
*/
export function isOdd(num) {
	return num % 2 !== 0
}

/**
 * @name: isEven
 * @cname: 判断是否是偶数
 * @desc: 以传入的数字判断是否为偶数 正确为true 否为flase
 * @param {*} num 
 * @panme: num=20
 * @result: true
*/
export function isEven(num) {
	return !isOdd(num)
}
