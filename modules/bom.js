/** Bom模块 */
import {
    convertStr2Obj
} from './object'

/**
 * @name: getQueryParams
 * @cname: 获取查询参数
 * @desc: 读取网页地址跟在问号后面的部分
 * @panme: https://www.baidu.com/s?ie=UTF-8&wd=%E7%99%BE%E5%BA%A6
 * @result: {ie: 'UTF-8', wd: '%E7%99%BE%E5%BA%A6'}
*/
export function getQueryParams() {
    if (!location.search) return null
    var query = location.search.replace("?", '')
    return convertStr2Obj(query, '&')
}

/**
 * @name: loadScript
 * @cname: 加载script
 * @desc: 加载script
 * @param {Array} url callback
*/
export function loadScript(url, callback) {
    const head = document.querySelector("head")
    var script = document.createElement("script")
    script.src = `${url}?${Math.random()}`
    script.async = true
    if (callback) script.onload = callback
    head.appendChild(script)
}

/**
 * @name: calcPostion
 * @cname: 防div出画面
 * @desc: 用于拖动div的场景，获取最终位置，防止出画面
 * @param {*} left top maxLeft maxTop minLeft minTop 
 * @panme: 输入左上以及最大左上参数 最小左和最小上位0
 * @result: 防止div被拖出画面
*/
export function calcPostion(left, top, maxLeft, maxTop, minLeft = 0, minTop = 0) {
    if (left < minLeft) left = minLeft
    else if (left > maxLeft) left = maxLeft
    if (top < minTop) top = minTop
    else if (top > maxTop) top = maxTop
    return {
        left,
        top
    }
}

/**
 * @name: convertRem2Px
 * @cname: rem转化
 * @desc: 以当前浏览器的rem计算出px
 * @param {Array} rem
 * @panme: rem=1
 * @result: 16
*/
export function convertRem2Px(rem) {
    let fontSize = window.getComputedStyle(document.documentElement).fontSize
    return rem * parseFloat(fontSize.replace("px", ""))
}