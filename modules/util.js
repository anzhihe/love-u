/** 常用模块 */

/**
 * @name: repair0
 * @cname: 补0
 * @desc: 多用于时间方面 某月某日 往前补0
 * @param {*}  num
 * @panme: num=1
 * @result: 01
*/
export function repair0(num) {
    num += ''
    return num[1] ? num : '0' + num
}

/**
 * @name: last
 * @cname: 取最后值
 * @desc: 取数组或者字符串的最后一个值
 * @param {*}  input
 * @panme: input=[1,2,3]
 * @result: 3
*/
export let last = input => input[input.length - 1]

/**
 * @name: first
 * @cname: 取开头值
 * @desc: 取数组或者字符串的开头值
 * @param {*}  input
 * @panme: input=[1,2,3]
 * @result: 1
*/
export let first = input => input[0]

/**
 * @name: isUndef
 * @cname: 判断参数是否定义
 * @desc: 判断参数是否定义 定义了则为flase 否则为true
 * @param {*}  obj
 * @panme: obj=[1]
 * @result: flase
*/
export let isUndef = obj => obj === undefined

/**
 * @name: isNullOrUndef
 * @cname: 判断参数是否定义
 * @desc: 判断参数是否定义 定义了则为true 否则为flase
 * @param {*}  obj
 * @panme: obj=[1]
 * @result: true
*/
export let isNullOrUndef = obj => obj === undefined || obj === null

/**
 * @name: isArray
 * @cname: 判断是否是数组类型
 * @desc: 判断是否是数组类型
 * @param {*}  obj
 * @panme: obj=[1,2,3,4,5]
 * @result: true
*/
export let isArray = (obj) => toString(obj) === '[object Array]'

/**
 * @name: isFuction
 * @cname: 是否是函数
 * @desc: 是否是函数 
 * @param {*}  obj
 * @panme: function b(a) {return a} 
 * @result: true
*/
export let isFuction = (obj) => toString(obj) === '[object Function]'

/**
 * @name: toString
 * @cname: 检测对象类型
 * @desc: 检测对象类型根据传入的数据输出该数据的类型
 * @param {*}  obj
 * @panme: obj=[1,2,3,4,5] 
 * @result: '[object Array]'
*/
export let toString = (obj) => Object.prototype.toString.call(obj)